/**
 * 
 */
package loker;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Taufiq Kun
 *
 */
public class loker {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int jumLoker = 0;
		String userInput;
		do {
			try {
				System.out.printf("Input jumlah loker : ");
	            userInput = br.readLine();
	            jumLoker = Integer.parseInt(userInput);
	        }
	        catch (NumberFormatException e) {
	        	System.out.println("Input harus angka..!\n");
	        }
		} while (jumLoker < 1 );
		
		System.out.println("============================================= ");
		System.out.println("Jumlah Loker = " +jumLoker);
		System.out.println("you can start using this loker now !!");
		System.out.println("List command : ");
		System.out.println("1. input loker               -> input [space] Tipe Identitas [space] No.Identitas || ex : input sim 1111");
		System.out.println("2. leave loker               -> leave [space] nomer Loker || ex : leave 1");
		System.out.println("3. cek status loker          -> status");
		System.out.println("4. cari No.Identitas         -> find [space] No.identitas || ex : find 1111");
		System.out.println("5. cari nomor Tipe Identitas -> search [space] Tipe Indetitas || ex : search SIM ");
		System.out.println("============================================= \n");
		
		List<dataLoker> dataLoker = new ArrayList<dataLoker>();
		for (int i = 0; i < jumLoker; i++) {
			dataLoker d = new dataLoker();
			d.id = i+1; d.tipe=""; d.nomor="";
			dataLoker.add(d);
		}
		
		String input = null;
		do {
			input = br.readLine();
			String[] splited = input.trim().split(" ");
			String key = splited[0].toLowerCase();
			if (splited.length == 3) {
				if ( key.equals("input") && !isNumeric(splited[1]) && isNumeric(splited[2])) {
				String tipe = splited[1];
				String nomor = splited[2];
				simpan(dataLoker, tipe, nomor);
				}
				else { System.out.println("Wrong command .. ! \n");}
			}
			
			else if (key.equals("status")) {
				status(dataLoker);
			}
			
			else if (splited.length == 2 && !dataLoker.isEmpty()) {
				String param = splited[1];
				if(isNumeric(splited[1])) {
					if (key.equals("leave")) {
						remove(Integer.parseInt(splited[1]), dataLoker);
					} 
					else if(key.equals("find")) {
						find(param, dataLoker);
					}
					else { System.out.println("Wrong command .. ! \n");}
				}
				else {
					if(key.equals("search")) {
						search(param, dataLoker);
					}
					else { System.out.println("Wrong command .. ! \n");}
				}
				
			}
			else { System.out.println("Wrong command .. ! \n");}
		} 	while ( !input.equals("exit") );
		System.out.println("\n Program End ...");
	}
	
	private static boolean isNumeric(String str) { 
		try {  
			Integer.parseInt(str);  
			return true;
		} catch(NumberFormatException e){  
			return false;  
		}  
	}
	
	private static void simpan(List<dataLoker> dataLoker, String tipe, String nomor) {
		List<Integer> listId = new ArrayList<Integer>();
		dataLoker data = new dataLoker();
		for (dataLoker d : dataLoker) {
			if(d.nomor == ""){
				listId.add(d.id);
			}
		}
		
		if(listId.size() < 1) {
			System.out.println("Loker penuh.. ! \n");
		} else {
			int id = listId.get(0);
			data.id = id;
			data.tipe = tipe;
			data.nomor = nomor;
			dataLoker.set(id-1, data);
			System.out.println("Kartu identitas tersimpan di loker "+id+"\n");
		}
	}
	
	private static void status(List<dataLoker> dataLoker) {
		System.out.println("=================================");
		for (dataLoker s : dataLoker) {
			System.out.println("no. Loker       : "+s.id);
			System.out.println("Tipe Identitas  : "+s.tipe);
			System.out.println("No. Identitas   : "+s.nomor);
			System.out.println("=================================");
		}
		System.out.println("\n");
	}
	
	private static void remove(int index, List<dataLoker> dataLoker) {
		int no = index - 1;
		if (no < 0) {
			System.out.println("Loker tidak ada .. !\n");
		} else {
			dataLoker.get(no).tipe = "";
			dataLoker.get(no).nomor = "";
			System.out.println("Loker no "+index+" berhasil dikosongkan \n");
		}
	}
	
	private static void find(String param, List<dataLoker> dataLoker) {
		List<Integer> id = new ArrayList<Integer>();
		for (dataLoker d : dataLoker) {
			if(d.nomor.equals(param)) {
				id.add(d.id);
			}
		}
		if(id.isEmpty()) {
			System.out.println("No Identitas "+ param +" tidak ditemukan .. ! \n");
		}
		else {
			for (Integer i : id) {
				System.out.println("No Identitas "+ param +" berada di loker "+i +"\n");
			}
		}
	}
	
	private static void search(String param, List<dataLoker> dataLoker) {
		String str = "";
		for (dataLoker d : dataLoker) {
			if ((d.tipe.toLowerCase()).equals(param.toLowerCase())) {
				  str +=  d.nomor + ", ";
			}
		}
		if (str.length() > 1) {
			str = str.substring(0, str.length()-2);
			System.out.println("No Identitas ditemukan : "+ str +"\n");
		} else {
			System.out.println("No Identitas tidak ditemukan ! \n");
		}
		
	}

}
